# OpenML dataset: Dota2-Games-Results-Data-Set

https://www.openml.org/d/45563

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dota 2 is a popular computer game with two teams of 5 players. At the start of the game each player chooses a unique hero with different strengths and weaknesses.

Source:

stephen.tridgell '@' sydney.edu.au


Data Set Information:

Dota 2 is a popular computer game with two teams of 5 players. At the start of the game each player chooses a unique hero with different strengths and weaknesses. The dataset is reasonably sparse as only 10 of 113 possible heroes are chosen in a given game. All games were played in a space of 2 hours on the 13th of August, 2016

The data was collected using: [Web Link]


Attribute Information:

Each row of the dataset is a single game with the following features (in the order in the vector):
1. Team won the game (1 or -1)
2. Cluster ID (related to location)
3. Game mode (eg All Pick)
4. Game type (eg. Ranked)
5 - end: Each element is an indicator for a hero. Value of 1 indicates that a player from team '1' played as that hero and '-1' for the other team. Hero can be selected by only one player each game. This means that each row has five '1' and five '-1' values.

The hero to id mapping can be found here: [Web Link]

Relevant Papers:

N/A

Note:

This dataset merges the training and test data from UCI.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45563) of an [OpenML dataset](https://www.openml.org/d/45563). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45563/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45563/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45563/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

